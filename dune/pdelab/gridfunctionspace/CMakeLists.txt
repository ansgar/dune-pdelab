install(FILES compositegridfunctionspace.hh
              datahandleprovider.hh
              entityindexcache.hh
              genericdatahandle.hh
              gridfunctionadapter.hh
              gridfunctionspace.hh
              gridfunctionspacebase.hh
              gridfunctionspaceutilities.hh
              interpolate.hh
              lfsindexcache.hh
              localfunctionspace.hh
              localfunctionspacetags.hh
              localvector.hh
              powercompositegridfunctionspacebase.hh
              powergridfunctionspace.hh
              subspace.hh
              subspacelocalfunctionspace.hh
              tags.hh
              vectorgridfunctionspace.hh
              vtk.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/pdelab/gridfunctionspace)
